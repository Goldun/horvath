package main

import (
	"flag"
	"log"
	"math"
	"strings"

	"gitlab.com/Goldun/horvath/healthchecker"
	"gitlab.com/Goldun/horvath/logger"
	"gitlab.com/Goldun/horvath/notifier"
	"gitlab.com/Goldun/horvath/notifier/telegram"
	"gitlab.com/Goldun/horvath/quarantine"
)

var (
	URLS     = flag.String("urls", "", "url list splitted by ','")
	LABELS   = flag.String("labels", "", "label list (key-value) splitted by ',' (%2)")
	INTERVAL = flag.Int64("interval", 30, "interval scrape in second")
	INSECURE = flag.Bool("insecure", false, "insecure https")

	CHANNEL = flag.String("channel", "telegram", "notify channel")

	TELEGRAM_TOKEN   = flag.String("telegram_token", EMPTY_FLAG_STRING, "telegram bot token")
	TELEGRAM_ROOM_ID = flag.Int64("telegram_room_id", EMPTY_FLAG_INT, "telegram room id")

	EMPTY_FLAG_STRING       = "e"
	EMPTY_FLAG_INT    int64 = -1

	urls   []string
	labels []string
)

func init() {
	flag.Parse()

	urls = strings.Split(*URLS, ",")
	labels = strings.Split(*LABELS, ",")

	if math.Mod(float64(len(labels)), 2) != 0 {
		log.Fatal("labels is not key-value list")
	}

	if *CHANNEL == "telegram" && *TELEGRAM_TOKEN == EMPTY_FLAG_STRING || *TELEGRAM_ROOM_ID == EMPTY_FLAG_INT {
		log.Fatal("bad telegram settings")
	}
}

func main() {
	logger := logger.New()
	quarantine := quarantine.New()

	quarantine.Subscribe()
	go quarantine.Revise()
	go quarantine.Alert()

	notifier := notifier.New().
		SetLogger(logger)

	if *CHANNEL == "telegram" {
		telegram, err := telegram.New().
			SetRoomID(*TELEGRAM_ROOM_ID).
			SetToken(*TELEGRAM_TOKEN).
			Init()
		if err != nil {
			log.Fatal(err)
		}

		notifier.
			SetChannel(telegram).
			SetFromQuarantine(quarantine).
			Start()
	}

	healthchecker.New().
		SetURLS(urls...).
		SetLabels(labels...).
		SetInsecure(*INSECURE).
		SetLogger(logger).
		SetInterval(*INTERVAL).
		SetQuarantine(quarantine).
		Run()
}
