package quarantine

import (
	"time"

	"gitlab.com/Goldun/horvath/healthchecker/target"
)

const (
	STATE_UNKNOWN  = ""
	STATE_ALERTING = "ALERTING"
	STATE_OK       = "OK"
	STATE_DELETED  = "DELETED"
)

type Quarantine struct {
	Sick    chan ([]target.ActiveTarget)
	Healthy chan ([]target.ActiveTarget)

	ChannelOK       chan (target.ActiveTarget)
	ChannelAlerting chan (target.ActiveTarget)
	ChannelDeleted  chan (target.ActiveTarget)

	sickTargets    map[string]target.ActiveTarget
	healthyTargets map[string]target.ActiveTarget
}

func New() *Quarantine {
	return &Quarantine{
		Sick:            make(chan []target.ActiveTarget),
		Healthy:         make(chan []target.ActiveTarget),
		sickTargets:     make(map[string]target.ActiveTarget),
		healthyTargets:  make(map[string]target.ActiveTarget),
		ChannelOK:       make(chan target.ActiveTarget),
		ChannelAlerting: make(chan target.ActiveTarget),
		ChannelDeleted:  make(chan target.ActiveTarget),
	}
}

func (quarantine *Quarantine) Subscribe() {
	go func() {
		for _, sick := range <-quarantine.Sick {
			quarantine.sickTargets[sick.ScrapeURL] = sick
		}
	}()
	go func() {
		for _, healthy := range <-quarantine.Healthy {
			quarantine.healthyTargets[healthy.ScrapeURL] = healthy
		}
	}()
}

func (quarantine *Quarantine) Revise() {
	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()
	for ; true; <-ticker.C {
		for sickURL, sickTarget := range quarantine.sickTargets {
			if sickTarget.Time.Before(time.Now().Add(-24 * time.Hour)) {
				delete(quarantine.sickTargets, sickURL)

				var target = sickTarget
				target.State = STATE_DELETED

				quarantine.ChannelDeleted <- target
			}
			for healthyURL := range quarantine.healthyTargets {
				if sickURL == healthyURL {
					delete(quarantine.sickTargets, sickURL)

					var target = sickTarget
					target.State = STATE_OK

					quarantine.ChannelOK <- target
				}
			}
		}
	}
}

func (quarantine *Quarantine) Alert() {
	ticker := time.NewTicker(30 * time.Second)
	defer ticker.Stop()
	for ; true; <-ticker.C {
		for url, sick := range quarantine.sickTargets {
			if sick.State != STATE_ALERTING {
				var target = quarantine.sickTargets[url]
				target.State = STATE_ALERTING
				quarantine.sickTargets[url] = target

				quarantine.ChannelAlerting <- target
			}
		}
	}
}
