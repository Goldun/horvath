package notifier

import (
	"gitlab.com/Goldun/horvath/healthchecker/target"
	"gitlab.com/Goldun/horvath/quarantine"
)

type Notifier struct {
	channelOK       chan (target.ActiveTarget)
	channelAlerting chan (target.ActiveTarget)
	channelDeleted  chan (target.ActiveTarget)

	logger logger

	notifyChannel NotifyChannel
}

type NotifyChannel interface {
	Notify(target.ActiveTarget) error
}

type logger interface {
	Log(interface{})
}

func New() *Notifier {
	return new(Notifier)
}

func (notifier *Notifier) SetFromQuarantine(quarantine *quarantine.Quarantine) *Notifier {
	notifier.channelOK = quarantine.ChannelOK
	notifier.channelAlerting = quarantine.ChannelAlerting
	notifier.channelDeleted = quarantine.ChannelDeleted

	return notifier
}

func (notifier *Notifier) SetLogger(logger logger) *Notifier {
	notifier.logger = logger
	return notifier
}

func (notifier *Notifier) SetChannel(channel NotifyChannel) *Notifier {
	notifier.notifyChannel = channel
	return notifier
}

func (notifier *Notifier) Start() {
	go func() {
		for target := range notifier.channelAlerting {
			if err := notifier.notifyChannel.Notify(target); err != nil {
				notifier.logger.Log(err)
			}
		}
	}()
	go func() {
		for target := range notifier.channelOK {
			if err := notifier.notifyChannel.Notify(target); err != nil {
				notifier.logger.Log(err)
			}
		}
	}()
	go func() {
		for target := range notifier.channelDeleted {
			if err := notifier.notifyChannel.Notify(target); err != nil {
				notifier.logger.Log(err)
			}
		}
	}()
}
