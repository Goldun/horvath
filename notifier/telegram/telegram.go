package telegram

import (
	"fmt"

	"gitlab.com/Goldun/horvath/healthchecker/target"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type NotifyChannel struct {
	token  string
	roomID int64
	bot    *tgbotapi.BotAPI
}

func New() *NotifyChannel {
	return new(NotifyChannel)
}

func (nc *NotifyChannel) SetToken(token string) *NotifyChannel {
	nc.token = token
	return nc
}

func (nc *NotifyChannel) SetRoomID(roomID int64) *NotifyChannel {
	nc.roomID = roomID
	return nc
}

func (nc *NotifyChannel) Init() (*NotifyChannel, error) {
	bot, err := tgbotapi.NewBotAPI(nc.token)
	if err != nil {
		return nil, err
	}
	nc.bot = bot

	return nc, nil
}

func (nc *NotifyChannel) Notify(target target.ActiveTarget) error {
	_, err := nc.bot.Send(
		tgbotapi.NewMessage(
			nc.roomID,
			fmt.Sprintf("State: %v \nURL: %v \nTime: %v \nError: %v\nMeta: %v", target.State, target.ScrapeURL, target.Time, target.Error, target.DiscoveredLabels),
		),
	)
	if err != nil {
		return err
	}

	return nil
}
