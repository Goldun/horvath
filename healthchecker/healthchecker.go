package healthchecker

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"math"
	"net"
	"net/http"
	"reflect"
	"time"

	"gitlab.com/Goldun/horvath/healthchecker/target"
	"gitlab.com/Goldun/horvath/quarantine"
)

const (
	HEALTH_UP = "up"
)

type HealthChecker struct {
	interval   int64
	urls       []string
	labels     []string
	logger     logger
	quarantine *quarantine.Quarantine
	httpClient *http.Client
}

type logger interface {
	Log(interface{})
}

func New() *HealthChecker {
	return new(HealthChecker)
}

func (hc *HealthChecker) SetInterval(interval int64) *HealthChecker {
	hc.interval = interval
	return hc
}

func (hc *HealthChecker) SetURLS(urls ...string) *HealthChecker {
	hc.urls = urls
	return hc
}

func (hc *HealthChecker) SetInsecure(insecure bool) *HealthChecker {
	hc.httpClient = func() *http.Client {
		timeout := 60 * time.Second
		transport := &http.Transport{
			TLSHandshakeTimeout:   5 * time.Second,
			MaxConnsPerHost:       200,
			MaxIdleConnsPerHost:   200,
			ResponseHeaderTimeout: timeout,
			DialContext: (&net.Dialer{
				Timeout:   timeout,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			TLSClientConfig: &tls.Config{InsecureSkipVerify: insecure},
		}
		return &http.Client{Transport: transport, Timeout: timeout}
	}()

	return hc
}

func (hc *HealthChecker) SetLabels(labels ...string) *HealthChecker {
	hc.labels = labels
	return hc
}

func (hc *HealthChecker) SetLogger(logger logger) *HealthChecker {
	hc.logger = logger
	return hc
}

func (hc *HealthChecker) SetQuarantine(quarantine *quarantine.Quarantine) *HealthChecker {
	hc.quarantine = quarantine
	return hc
}

func (hc *HealthChecker) Run() {
	ticker := time.NewTicker(time.Duration(hc.interval) * time.Second)
	defer ticker.Stop()
	for ; true; <-ticker.C {
		for _, url := range hc.urls {
			request, err := http.NewRequest(http.MethodGet, url, nil)
			if err != nil {
				hc.logger.Log(err)
				continue
			}

			response, err := hc.httpClient.Do(request)
			if err != nil {
				hc.logger.Log(err)
				continue
			}

			if response.StatusCode < 200 || response.StatusCode > 299 {
				hc.logger.Log("responce status code is " + response.Status)
				continue
			}

			body, err := ioutil.ReadAll(response.Body)
			if err != nil {
				hc.logger.Log(err)
				continue
			}

			var targets target.Targets

			if err := json.Unmarshal(body, &targets); err != nil {
				hc.logger.Log(err)
				continue
			}

			var sick []target.ActiveTarget
			var healthy []target.ActiveTarget

			var filterredTargets []target.ActiveTarget

			for _, target := range targets.Data.ActiveTargets {
				var labelKeys []string
				var labelValues []string

				for index, key := range hc.labels {
					if math.Mod(float64(index+1), 3) == 0 || index == 0 {
						labelKeys = append(labelKeys, key)
					}
				}
				for index, value := range hc.labels {
					if math.Mod(float64(index+1), 2) == 0 {
						labelValues = append(labelValues, value)
					}
				}

				for _, key := range labelKeys {
					for labelKey, labelValue := range target.DiscoveredLabels {
						if reflect.TypeOf(labelValue).Kind() == reflect.String {
							for _, value := range labelValues {
								if value == labelValue && key == labelKey {
									filterredTargets = append(filterredTargets, target)
								}
							}
						}
					}
				}
			}

			for _, target := range filterredTargets {

				if target.Health != HEALTH_UP {
					sick = append(sick, target)
				} else {
					healthy = append(healthy, target)
				}
			}

			hc.quarantine.Sick <- sick
			hc.quarantine.Healthy <- healthy
		}
	}
}
