package target

import "time"

type Targets struct {
	Data Data `json:"data"`
}

type Data struct {
	ActiveTargets []ActiveTarget `json:"activeTargets"`
}

type ActiveTarget struct {
	DiscoveredLabels map[string]interface{} `json:"discoveredLabels"`
	Labels           map[string]interface{} `json:"labels"`
	ScrapeURL        string                 `json:"scrapeUrl"`
	Error            string                 `json:"lastError"`
	Time             time.Time              `json:"lastScrape"`
	Health           string                 `json:"health"`
	State            string                 `json:"-"`
}
