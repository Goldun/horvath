# Horvath

Horvath created for monitoring prometheus endpoints(targets) and notifying about fails.

    Available flags:

    -urls="http://prometheus:9090/api/v1/targets" //[]url for scrape (separator ",")
    -labels="__meta_kubernetes_namespace,default" //[]filters labels (key,value)
    -interval=10 //scrape interval
    
    Channel flags:

        -channel="telegram" //notify channel
        
    Telegram channel flags:

        -telegram_token="token" //telegram bot token
        -telegram_room_id="-10000000000" //telegram room(chat) ID