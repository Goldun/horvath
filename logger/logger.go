package logger

import "log"

type Logger struct{}

func New() *Logger {
	return new(Logger)
}

func (logger *Logger) Log(l interface{}) {
	log.Println(l)
}
